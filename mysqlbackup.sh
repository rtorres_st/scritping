#!/bin/bash 

# backup script

echo "backup script"

# MYSQLDUMP="docker exec mysqldb /usr/bin/mysqldump"
MYSQLDUMP="/usr/bin/mysqldump"

MYSQL_USER=""
MYSQL_PASSWORD=""
echo "Enter MySql host to backup (Dns Name)"
read MYSQL_HOST
BACKUP_FILE_PATH="/home/$USER/Documents/dev/work/backups"
SCHEMAS_TO_BACKUP=("real_time_reporting" "ais_service" "cesb_service" "iot" "mis_service" "pooled_sorter" "reportingservice" "sort_plan" "st_utils" "tag_service")
MYSQL_BACKUP_OPTIONS="--events --triggers --routines --lock-tables=false"

mkdir -p "$BACKUP_FILE_PATH"

for SCHEMA_NAME in "${SCHEMAS_TO_BACKUP[@]}"
do
    echo "Backing up $SCHEMA_NAME"
    $MYSQLDUMP -h$MYSQL_HOST -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_BACKUP_OPTIONS $SCHEMA_NAME > $BACKUP_FILE_PATH/$SCHEMA_NAME.sql
done
