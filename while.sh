#!/bin/bash

COUNT=0

while [ $COUNT -lt 1000000000000000000 ]
do
    echo $COUNT
    ((COUNT++))
done

return 0