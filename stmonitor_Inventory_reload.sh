#!/usr/bin/env bash

environments=( DEV1 DEV2 DEV3 TEST1 TEST2 TEST3 QA1 QA2 QA3 UAT1 UAT2 UAT3 )

for env in "${environments[@]}"
do
   ssh $env  'whoami;  python /opt/stdevops/bin/docker-stats.py;' \
    'python /opt/stdevops/bin/inventory.py; '\
    'python /opt/stdevops/bin/system_health_check.py; '\
    'python /opt/stdevops/bin/disk_space_used.py; '\
    'python /opt/stdevops/bin/docker-stats.py; '\
    'python /opt/stdevops/bin/inventory.py; '\
    'python /opt/stdevops/bin/memory_check.py; '\
    'python /opt/stdevops/bin/socket_counts.py; '\
    'python /opt/stdevops/bin/system_health_check.py'
echo "$env is done "
done
