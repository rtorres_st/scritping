 #!/bin/bash -x      
   
   service=(base_stack sensorthink-tools nodeframework wes_server
       tag_service tag_service sort-plan-service 
       sensorthink-commons reporting_service pooled-sorter
       modgraph_metadata_service mis-service iot_platform_server 
       iot_common fsm_engine cesb-service ais-service devops 
       configurations stmonitor module-graph-service 
       elk-stack loadbalancer wes_client analytics_web
        )

   for s in ${service[@]};
   do
    git clone git@bitbucket.org:tompkinsinc/${s}.git
   done 
    
