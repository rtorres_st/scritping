#!/bin/bash

aws elbv2 modify-target-group \
--target-group-arn arn:aws:elasticloadbalancing:us-west-2:123456789012:targetgroup/my-https-targets/2453ed029918f21f \
--health-check-protocol HTTPS --health-check-port 80